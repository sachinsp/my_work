# importing all required lib

import pandas as pd
import numpy as np
import sqlite3
from sqlalchemy import create_engine
from datetime import datetime
import os
import mysql.connector
import json
from kafka import KafkaConsumer, KafkaProducer
import requests

# DATABASE NAME
Data_base = "weather_data_schema"
row_data = []
col_name = []
kafka_topic = "quickstart-events"
table_name = "weather_data"
url_druid_supervisor = 'http://localhost:8081/druid/indexer/v1/supervisor'
headers = {"Content-Type": 'application/json'}

# CONNECTING PYTHON AND MYSQL
conn = mysql.connector.connect(
    host="localhost",
    user="root",
    password="Suma_1994",
    database=Data_base
)

my_cursor = conn.cursor()


# GETTING DATABASES NAME
def get_db():
    my_cursor.execute(" SHOW DATABASES ")
    my_result = my_cursor.fetchall()
    for i in my_result:
        print(i[0])


# GETTING COLUMNS FROM TABLE
# SELECT * FROM employees
# SELECT * FROM customers;

def get_col_name():
    my_cursor.execute(f"SHOW COLUMNS FROM {Data_base}.{table_name};")
    my_result = my_cursor.fetchall()
    for i in my_result:
        col_name.append(i[0])


# GETTING DATA FROM TABLE

def get_data():
    my_cursor.execute(f"SELECT * FROM {table_name}; ")
    my_result = my_cursor.fetchall()
    for i in my_result:
        row_data.append(i)


# COLLECTING ALL DATA AND ADDING IT TO PANDAS.DATAFRAME AND CONVERTING TO JSON

def convert_csv_to_json():
    df = pd.DataFrame(row_data, columns=col_name)
    df.to_csv("data_for_kafka_csv", index=False)

    csv_file = pd.DataFrame(pd.read_csv("data_for_kafka_csv"))
    csv_file.to_json("file_for_kafka.json", orient="records", date_format="epoch", double_precision=10,
                     force_ascii=True,
                     date_unit="ms", default_handler=None)


# GET THE JSON DATA
def get_json_data():
    with open('file_for_kafka.json') as f:
        data = json.load(f)
        return data


# ADDING ALL DATA TO KAFKA TOPIC
# def json_serializer(data):
# return json.dumps(data).encode('utf-8')


# producer = KafkaProducer(value_serializer=lambda v: json.dumps(v).encode('utf-8'))

producer = KafkaProducer(bootstrap_servers='localhost:9092',
                         value_serializer=lambda v: json.dumps(v).encode('utf-8'))

# DRUID DATA SPEC
data_spec_kafka = '''
{
  "type": "kafka",
  "spec": {
    "ioConfig": {
      "type": "kafka",
      "consumerProperties": {
        "bootstrap.servers": "localhost:9092"
      },
      "topic": "quickstart-events",
      "inputFormat": {
        "type": "json"
      },
      "useEarliestOffset": true
    },
    "tuningConfig": {
      "type": "kafka"
    },
    "dataSchema": {
      "dataSource": "my_kafka_data_to_druid",
      "granularitySpec": {
        "type": "uniform",
        "queryGranularity": "NONE",
        "segmentGranularity": "HOUR",
        "rollup": false
      },
      "timestampSpec": {
        "column": "datetime",
        "format": "auto"
      },
      "dimensionsSpec": {
        "dimensions": [
          "address",
          {
            "type": "double",
            "name": "latitude"
          },
          {
            "type": "double",
            "name": "longitude"
          },
          {
            "type": "long",
            "name": "maxt"
          },
          {
            "type": "long",
            "name": "mint"
          },
          {
            "type": "double",
            "name": "precip"
          },
          {
            "type": "long",
            "name": "pressure"
          },
          {
            "type": "double",
            "name": "temp"
          },
          {
            "type": "long",
            "name": "wdir"
          },
          {
            "type": "double",
            "name": "wgust"
          },
          {
            "type": "double",
            "name": "wspd"
          }
        ]
      }
    }
  }
}'''


def kafka_to_druid(url, data_spec_kafka, headers):

    response = requests.post(url, data=data_spec_kafka, headers=headers)
    print(response.text)


if __name__ == "__main__":
    # get_db()
    get_col_name()
    print("got col's name..")
    get_data()
    print("got row data..")
    convert_csv_to_json()
    print("converted to csv to json...")
    user_data = get_json_data()
    print("made json file..")
    print(f"len of data is : {len(user_data)}")
    for user in user_data:
        producer.send(kafka_topic, user)
        producer.flush()
    print("data sent to kafka")
    kafka_to_druid(url_druid_supervisor, data_spec_kafka, headers)
    print("data sent from kafka to Druid..")
