import requests
import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

overlord_port = "http://localhost:8081"
coordinator_port = "http://localhost:8081"
router_port = "http://localhost:8888"
broker_port = "http://localhost:8082"
historical_port = "http://localhost:8083"
middle_manager = "http://localhost:8091"

default_url_susp = "/druid/indexer/v1/supervisor"
default_url_local = "/druid/indexer/v1/task/"

sql_url_b = f"{broker_port}/druid/v2/?pretty"
sql_url_r = f"{router_port}/druid/v2/?pretty"
sql_url_sr = f"{router_port}/druid/v2/sql"
sql_url_sb = f"{broker_port}/druid/v2/sql"
h1 = {"Content-Type": "application/json",
      "Accept" : "application/json"
      # "Accept": "application/x-jackson-smile"
      }



data_sql_query = '''
{
    "query" : " SELECT * FROM my_local_data_to_druid WHERE temp >60 AND wdir >300 " 
}

'''

data_sqlquery = '''
{
    "queryType": "scan",
    "dataSource": {
        "type": "table",
        "name": "my_local_data_to_druid"
    },
    "intervals": {
        "type": "intervals",
        "intervals": [
            "-146136543-09-08T08:23:32.096Z/146140482-04-24T15:36:27.903Z"
        ]
    },
    "virtualColumns": [],
    "resultFormat": "compactedList",
    "batchSize": 20480,
    "limit": 100,
    "order": "none",
    "filter": {
        "type": "and",
        "fields": [
            {
                "type": "bound",
                "dimension": "temp",
                "lower": "60",
                "upper": null,
                "lowerStrict": true,
                "upperStrict": false,
                "extractionFn": null,
                "ordering": {
                    "type": "numeric"
                }
            },
            {
                "type": "bound",
                "dimension": "wdir",
                "lower": "300",
                "upper": null,
                "lowerStrict": true,
                "upperStrict": false,
                "extractionFn": null,
                "ordering": {
                    "type": "numeric"
                }
            }
        ]
    },
    "columns": [
        "__time",
        "address",
        "latitude",
        "longitude",
        "maxt",
        "mint",
        "precip",
        "pressure",
        "temp",
        "wdir",
        "wgust",
        "wspd"
    ],
    "legacy": false,
    "context": {
        "sqlOuterLimit": 100
    },
    "descending": false,
    "granularity": {
        "type": "all"
    }
}

'''



data_sql_query_1 = '''
 {
   "queryType": "scan",
   "dataSource": "my_local_data_to_druid",
   "resultFormat": "list",
   "columns":[],
   "intervals": [
     "-146136543-09-08T08:23:32.096Z/146140482-04-24T15:36:27.903Z"
   ]
 }
'''


def data_sql_query_post(url, sql_query,h1):
    response = requests.post(url, data=sql_query, headers=h1 )
    data = response.json()
    return data




# print(url)
get_data = data_sql_query_post(sql_url_r, data_sqlquery, h1)

# print(type(get_data))
# print(get_data)
# print(get_data['columns'])
# print(len(get_data))

seg_id = []
cols = []
event = []

for i in get_data:
    # print(i["segmentId"])
    seg_id.append(i["segmentId"])
    # print(i["columns"])
    cols.append(i["columns"])
    # print(i["events"][0])
    event.append(i["events"][0])



# print(seg_id)
cols_1 = [cols[0]]
# print(event)
# print(cols_1)



df = pd.DataFrame(event, columns= cols_1, index= seg_id)
print(df.describe())
# df.to_csv("result_csv", index=False)


# plt.plot(df.temp, df.wdir)
# plt.show()


# plt.plot(x= df["temp"], y=df['wdir'],
#         title='temp vs wdir')
# plt.ylabel('avg tweet length (chars)')
# plt.show()








'''
task_id = "index_kafka_my-quickstart-events-test-11_45785bbe7a34b18_ghonfbbd"

url = f"{overlord_port}{default_url_susp}/{task_id}/reports"

# print(url)

x = requests.get(url)
print(x.text)

'''

















#
# # getting status of task for local ingest
# task_id_1 = "index_parallel_local-wikipedia-001_nlfncbpn_2021-04-14T12:04:28.643Z"
#
# url = f"{overlord_port}/druid/indexer/v1/task/{task_id_1}/reports"
# # print(url)
# # print(overlord_port)
#
# x = requests.get(url)
# print(x.text)














'''
data = {
    "type": "kill",
    "id": "index_parallel_wikipedia-1_iiddiflf_2021-04-14T07:51:14.829Z",
    "dataSource": "wikipedia-1",
    "interval" : "",
    "context": ""
}


# headers = {"Content-Type": 'application/json'}
# response = requests.post(url, data=data, headers=headers)
# print(response.text)
'''
