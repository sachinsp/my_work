# importing all required lib

import pandas as pd
import numpy as np
import sqlite3
from sqlalchemy import create_engine
from datetime import datetime
import os
import mysql.connector
import json
from kafka import KafkaConsumer, KafkaProducer
import requests

# DATABASE NAME
Data_base = "weather_data_schema"
row_data = []
col_name = []
table_name = "weather_data"
url_druid_v1 = "http://localhost:8081/druid/indexer/v1/task"
headers = {"Content-Type": 'application/json'}


# CONNECTING PYTHON AND MYSQL
conn = mysql.connector.connect(
    host="localhost",
    user="root",
    password="Suma_1994",
    database=Data_base
)

my_cursor = conn.cursor()


# GETTING DATABASES NAME
def get_db():
    my_cursor.execute(" SHOW DATABASES ")
    my_result = my_cursor.fetchall()
    for i in my_result:
        print(i[0])


# GETTING COLUMNS FROM TABLE
# SELECT * FROM employees
# SELECT * FROM customers;

def get_col_name():
    my_cursor.execute(f"SHOW COLUMNS FROM {Data_base}.{table_name};")
    my_result = my_cursor.fetchall()
    for i in my_result:
        col_name.append(i[0])


# GETTING DATA FROM TABLE

def get_data():
    my_cursor.execute(f"SELECT * FROM {table_name}; ")
    my_result = my_cursor.fetchall()
    for i in my_result:
        row_data.append(i)


# COLLECTING ALL DATA AND ADDING IT TO PANDAS.DATAFRAME AND CONVERTING TO JSON

def convert_data_to_csv():
    df = pd.DataFrame(row_data, columns=col_name)
    df.to_csv("data_for_druid_csv", index=False)



data_to_druid_spec = '''
{
  "type": "index_parallel",
  "spec": {
    "ioConfig": {
      "type": "index_parallel",
      "inputSource": {
        "type": "local",
        "filter": "data_for_druid_csv",
        "baseDir": "/home/sachin/projects/my_work/big_dataengg/"
      },
      "inputFormat": {
        "type": "csv",
        "findColumnsFromHeader": true
      }
    },
    "tuningConfig": {
      "type": "index_parallel",
      "partitionsSpec": {
        "type": "dynamic"
      }
    },
    "dataSchema": {
      "dataSource": "my_local_data_to_druid",
      "granularitySpec": {
        "type": "uniform",
        "queryGranularity": "NONE",
        "rollup": false,
        "segmentGranularity": "HOUR"
      },
      "timestampSpec": {
        "column": "datetime",
        "format": "auto"
      },
      "dimensionsSpec": {
        "dimensions": [
          "address",
          {
            "type": "double",
            "name": "latitude"
          },
          {
            "type": "double",
            "name": "longitude"
          },
          {
            "type": "long",
            "name": "maxt"
          },
          {
            "type": "long",
            "name": "mint"
          },
          {
            "type": "double",
            "name": "precip"
          },
          {
            "type": "long",
            "name": "pressure"
          },
          {
            "type": "double",
            "name": "temp"
          },
          {
            "type": "long",
            "name": "wdir"
          },
          {
            "type": "double",
            "name": "wgust"
          },
          {
            "type": "double",
            "name": "wspd"
          }
        ]
      }
    }
  }
}
'''

def data_druid(url, data_to_druid_spec, headers):
    response = requests.post(url, data=data_to_druid_spec, headers=headers)
    print(response.text)


if __name__ == "__main__":
    get_col_name()
    print("got col's name")
    get_data()
    print("got row datat")
    convert_data_to_csv()
    print("data converted to csv")

    data_druid(url_druid_v1, data_to_druid_spec, headers)
    print("sending data to druid from local to druid db")
    print("sent")


