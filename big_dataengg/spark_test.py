from pyspark.sql import *
from pyspark import SparkContext, SparkConf
from datetime import datetime, date
import pandas as pd
from pyspark.sql import Row



# spark = SparkSession.builder.getOrCreate()
# spark = SparkSession.builder \
#     .master("local[1]") \
#     .appName("Spark_test") \
#     .getOrCreate()


def init_spark(app_name, master_config):
    """
    :params app_name: Name of the app
    :params master_config: eg. local[4]
    :returns SparkContext, SQLContext, SparkSession:
    """
    conf = (SparkConf().setAppName(app_name).setMaster(master_config))

    sc = SparkContext(conf=conf)
    sc.setLogLevel("ERROR")
    sql_ctx = SQLContext(sc)
    spark = SparkSession(sc)

    return (sc, sql_ctx, spark)

sc, sql_ctx, spark = init_spark("test", "local[4]")




df = spark.read.option("header", "true").csv("data_file/2018.csv")

df.show()

# df.describe()

df.printSchema()









# rdd = sc.textFile('data_file/IBA.json')
# df2 = spark.read.json(rdd)
# df2.show()


# df = spark.read.load("data_file/IBA.json", format="json")

# df = spark.read.json("data_file/IBA.json")
# df = spark.read.csv("data_file/2018.csv")
#
# df = spark.read.option("header", "true").csv("data_file/2018.csv")


#
# df.printSchema()
#
# df.show()
# df.
# otherPeopleRDD = sc.parallelize("data_file/IBA.json")
# otherPeople = spark.read.json(otherPeopleRDD)
# otherPeople.show()


# df = pd.read_csv("data_file/2018.csv")
# print(df)


#
# df = spark.createDataFrame([
#     Row(a=1, b=2., c='string1', d=date(2000, 1, 1), e=datetime(2000, 1, 1, 12, 0)),
#     Row(a=2, b=3., c='string2', d=date(2000, 2, 1), e=datetime(2000, 1, 2, 12, 0)),
#     Row(a=4, b=5., c='string3', d=date(2000, 3, 1), e=datetime(2000, 1, 3, 12, 0))
# ])
#
# # print(df)
#
# df.show()


# df.printSchema()