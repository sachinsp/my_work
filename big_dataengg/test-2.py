from pydruid.client import *
import matplotlib.pyplot as plt
router_port = "http://localhost:8888"

sql_url_b = "/druid/v2/?pretty"


query = PyDruid("http://localhost:8888", "/druid/v2/?pretty")

ts = query.timeseries(
    datasource="my_local_data_to_druid",
    granularity='day',
    intervals='"-146136543-09-08T08:23:32.096Z/146140482-04-24T15:36:27.903'
)

df = query.export_pandas()

# df['timestamp'] = df['timestamp'].map(lambda x: x.split('T')[0])
# df.plot(x='timestamp', y='avg_tweet_length', ylim=(80, 140), rot=20,
#         title='Sochi 2014')
# plt.ylabel('avg tweet length (chars)')
# plt.show()